#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

func = input("Funktion eingeben: ")
intl = input("Linke Intervallgrenze eingeben: ")
intr = input("Rechte Intervallgrenze eingeben: ")
prec = input("Anzahl der Punkte im Intervall: ")
Xlabel = input("x-Achsenbeschriftung: ")
Ylabel = input("y-Achsenbeschriftung: ")

x = np.linspace(float(intl), float(intr), num=int(prec))
func = sp.sympify(func)
y = [func.subs('x', i) for i in x]
y = np.array(y)

plt.plot(x, y)
plt.xlabel(Xlabel)
plt.ylabel(Ylabel)
plt.grid()
plt.show()
