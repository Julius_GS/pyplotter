import matplotlib.pyplot as plt
import numpy as np
import sympy.sympify


class funcplotter:
    def __init__(XLabel, YLabel, intl, intr):
        self.functions = []
        self.numb = 0
        self.Xlabel = XLabel
        self.Ylabel = YLabel
        self.xvalues = np.linspace(intl, intr, num=prec)
        self.plot = True 
        self.legend = False
        self.legends = []

    def addfunc(func):
        func = sympify(func)
        self.functions.append(func)


